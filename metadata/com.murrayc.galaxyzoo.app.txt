Categories:Science & Education
License:GPLv3
Web Site:
Source Code:https://github.com/murraycu/android-galaxyzoo
Issue Tracker:https://github.com/murraycu/android-galaxyzoo/issues

Auto Name:Galaxy Zoo
Summary:Help to classify galaxies
Description:
Classify [http://www.galaxyzoo.org/ Galaxy Zoo] subjects. Official approved
by the [https://www.zooniverse.org/ Zooniverse project].

Asks you questions about a picture of a galaxy, with each question depending
on the previous question. This "Citizen Science" helps astronomers to analyze
the huge amount of images of galaxies provided, for instance, by the Hubble
Space Telescope.
.

Repo Type:git
Repo:https://github.com/murraycu/android-galaxyzoo

Build:1.24,24
    commit=1.24
    subdir=app
    gradle=yes
    prebuild=sed -i -e 's/21.0.2/21.0.+/g' build.gradle

Build:1.25,25
    commit=1.25
    subdir=app
    gradle=yes
    prebuild=sed -i -e 's/21.0.2/21.0.+/g' build.gradle

Build:1.26,26
    commit=1.26
    subdir=app
    gradle=yes
    prebuild=sed -i -e 's/21.0.2/21.0.+/g' build.gradle

Build:1.30,30
    disable=failing build
    commit=1.30
    subdir=app
    gradle=yes
    prebuild=sed -i -e 's/21.0.2/21.0.+/g' build.gradle

Build:1.31,31
    commit=1.31
    subdir=app
    gradle=yes
    prebuild=sed -i -e 's/21.0.2/21.0.+/g' -e 's/gradle:1.0.0-rc4/gradle:0.14.0/g'  build.gradle ../build.gradle

Build:1.32,32
    commit=1.32
    subdir=app
    gradle=yes
    prebuild=sed -i -e 's/21.0.2/21.0.+/g' -e 's/gradle:1.0.0-rc4/gradle:0.14.0/g'  build.gradle ../build.gradle

Build:1.33,33
    commit=1.33
    subdir=app
    gradle=yes
    prebuild=sed -i -e 's/21.0.2/21.0.+/g' -e 's/gradle:1.0.0-rc4/gradle:0.14.0/g'  build.gradle ../build.gradle

Build:1.34,34
    commit=1.34
    subdir=app
    gradle=yes
    prebuild=sed -i -e 's/21.0.2/21.0.+/g' -e 's/gradle:1.0.0-rc4/gradle:0.14.0/g'  build.gradle ../build.gradle

Build:1.35,35
    commit=1.35
    subdir=app
    gradle=yes
    prebuild=sed -i -e 's/21.0.2/21.0.+/g' -e 's/gradle:1.0.0-rc4/gradle:0.14.0/g'  build.gradle ../build.gradle

Build:1.36,36
    commit=1.36
    subdir=app
    gradle=yes
    prebuild=sed -i -e 's/21.0.2/21.0.+/g' -e 's/gradle:1.0.0-rc4/gradle:0.14.0/g'  build.gradle ../build.gradle

Build:1.37,37
    commit=1.37
    subdir=app
    gradle=yes
    prebuild=sed -i -e 's/21.0.2/21.0.+/g' -e 's/gradle:1.0.0-rc4/gradle:0.14.0/g'  build.gradle ../build.gradle

Build:1.39,39
    commit=1.39
    subdir=app
    gradle=yes
    prebuild=sed -i -e 's/21.0.2/21.0.+/g' -e 's/gradle:1.0.0-rc4/gradle:0.14.0/g'  build.gradle ../build.gradle

Build:1.40,40
    commit=1.40
    subdir=app
    gradle=yes
    prebuild=sed -i -e 's/21.0.2/21.0.+/g' -e 's/gradle:1.0.0-rc4/gradle:0.14.0/g'  build.gradle ../build.gradle

Build:1.41,41
    commit=1.41
    subdir=app
    gradle=yes
    prebuild=sed -i -e 's/21.0.2/21.0.+/g' -e 's/gradle:1.0.0-rc4/gradle:0.14.0/g'  build.gradle ../build.gradle

Build:1.42,42
    commit=1.42
    subdir=app
    gradle=yes
    prebuild=sed -i -e 's/21.0.2/21.0.+/g' -e 's/gradle:1.0.0-rc4/gradle:0.14.0/g'  build.gradle ../build.gradle

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.42
Current Version Code:42

