Categories:Office
License:MIT
Web Site:https://altnico.github.io/projects/android/Asura.html
Source Code:https://github.com/asura-app/android
Issue Tracker:https://github.com/asura-app/android/issues

Auto Name:Asura
Summary:Template for an app parsing a JSON file
Description:
The goal of Asura is to make it easy to build an individual app which
parses a JSON file and display it in a ListView, mainly just with
configuring a single file. To get further information, take a look
[https://github.com/asura-app/android/blob/master/README.md here].

[https://github.com/asura-app/android/releases Changelog]
.

Repo Type:git
Repo:https://github.com/asura-app/android

Build:0.30,4
    commit=0.30
    subdir=app/app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.30
Current Version Code:4
