Categories:Office,Internet
License:GPLv3
Web Site:https://code.google.com/p/android-open-in-browser
Source Code:https://code.google.com/p/android-open-in-browser/source
Issue Tracker:https://code.google.com/p/android-open-in-browser/source/list

Auto Name:Open in browser
Summary:Open local HTML files in a browser
Description:
Adds ability to open HTML/text/image files saved to SD card from a file
manager directly in the browser.
.

Repo Type:hg
Repo:https://code.google.com/p/android-open-in-browser/

Build:0.0.5,5
    commit=045fa60d5940

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:0.0.5
Current Version Code:5

