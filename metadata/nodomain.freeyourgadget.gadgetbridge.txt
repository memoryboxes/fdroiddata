Categories:System
License:AGPL
Web Site:https://github.com/Freeyourgadget/Gadgetbridge/blob/HEAD/README.md
Source Code:https://github.com/Freeyourgadget/Gadgetbridge
Issue Tracker:https://github.com/Freeyourgadget/Gadgetbridge/issues

Auto Name:Gadgetbridge
Summary:Communicate with your Pebble
Description:
Use your Pebble without the vendors closed source application and without
the need to create an account and transmit any of your data to the vendors
servers.

We plan to add support for the Mi Band and maybe even more devices.

Features:
* Incoming calls notification and display (caller, phone number)
* Outgoing call display
* Reject/hangup calls
* SMS notification (sender, body)
* K-9 Mail notification support (sender, subject, preview)
* Support for generic notificaions (above filtered out)
* Apollo music playback info (artist, album, track)
* Music control: play/pause, next track, previous track
.

Repo Type:git
Repo:https://github.com/Freeyourgadget/Gadgetbridge.git

Build:0.1.0,1
    commit=0.1.0
    subdir=app
    gradle=yes

Build:0.1.1,2
    commit=0.1.1
    subdir=app
    gradle=yes

Build:0.1.2,3
    commit=0.1.2
    subdir=app
    gradle=yes

Build:0.1.3,4
    commit=0.1.3
    subdir=app
    gradle=yes

Build:0.1.4,5
    commit=0.1.4
    subdir=app
    gradle=yes

Build:0.1.5,6
    commit=0.1.5
    subdir=app
    gradle=yes

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.1.5
Current Version Code:6

