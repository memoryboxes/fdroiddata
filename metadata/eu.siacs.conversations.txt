Categories:Internet
License:GPLv3
Web Site:
Source Code:https://github.com/siacs/Conversations
Issue Tracker:https://github.com/siacs/Conversations/issues
Donate:https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=CW3SYT3KG5PDL
FlattrID:3531619
Bitcoin:1NxSU1YxYzJVDpX1rcESAA3NJki7kRgeeu

Auto Name:Conversations
Summary:XMPP client
Description:
XMPP client designed with ease of use and security in mind.

Features:
* End-to-end encryption with either OTR or OpenPGP
* Sending and receiving images
* Indication when your contact has read your message
* Intuitive UI that follows Android Design guidelines
* Pictures / Avatars for your Contacts
* Syncs with desktop client
* Conferences (with support for bookmarks)
* Address book integration
* Multiple accounts / unified inbox
* Very low impact on battery life

Noteable XEPs:
* XEP-0065: SOCKS5 Bytestreams
* XEP-0163: Personal Eventing Protocol
* XEP-0191: Blocking Command
* XEP-0198: Stream Management
* XEP-0237: Roster Versioning
* XEP-0280: Message Carbons
* XEP-0313: Message Archive Management
* XEP-0352: Client State Indication

[https://github.com/siacs/Conversations/blob/development/CHANGELOG.md Changelog]
.

Repo Type:git
Repo:https://github.com/siacs/Conversations.git

Build:0.1.3,9
    disable=prebuild jars
    commit=0.1.3
    submodules=yes
    srclibs=Otr4j-jitsi@33d95bb3710986d048fb4cbe42dc991f9ddd9de2,BouncyCastle@r1rv50
    rm=libs/android-support-v4.jar
    extlibs=android/android-support-v4.jar
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Build:0.2.1,12
    commit=0.2.1
    submodules=yes
    srclibs=Otr4j-jitsi@30fbaea9388616b268de82be0267acf2441b218b,BouncyCastle@r1rv50
    rm=libs/bcprov-jdk15on-150.jar,libs/otr4j-0.10.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && \
        cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-150.jar libs/ && \
        pushd $$Otr4j-jitsi$$ && \
        mvn clean && \
        mvn package && \
        popd && \
        cp $$Otr4j-jitsi$$/target/otr4j-0.10.jar libs/
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Build:0.2.2,13
    commit=0.2.2
    submodules=yes
    srclibs=Otr4j-jitsi@30fbaea9388616b268de82be0267acf2441b218b,BouncyCastle@r1rv50
    rm=libs/bcprov-jdk15on-150.jar,libs/otr4j-0.10.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && \
        cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-150.jar libs/ && \
        pushd $$Otr4j-jitsi$$ && \
        mvn clean && \
        mvn package && \
        popd && \
        cp $$Otr4j-jitsi$$/target/otr4j-0.10.jar libs/
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Build:0.2.3,14
    commit=0.2.3
    submodules=yes
    srclibs=Otr4j-jitsi@30fbaea9388616b268de82be0267acf2441b218b,BouncyCastle@r1rv50
    rm=libs/bcprov-jdk15on-150.jar,libs/otr4j-0.10.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && \
        cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-150.jar libs/ && \
        pushd $$Otr4j-jitsi$$ && \
        mvn clean && \
        mvn package && \
        popd && \
        cp $$Otr4j-jitsi$$/target/otr4j-0.10.jar libs/
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Build:0.3,16
    commit=0.3
    submodules=yes
    srclibs=Otr4j-jitsi@30fbaea9388616b268de82be0267acf2441b218b,BouncyCastle@r1rv50
    rm=libs/bcprov-jdk15on-150.jar,libs/otr4j-0.10.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && \
        cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-150.jar libs/ && \
        pushd $$Otr4j-jitsi$$ && \
        mvn clean && \
        mvn package && \
        popd && \
        cp $$Otr4j-jitsi$$/target/otr4j-0.10.jar libs/
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Build:0.4,18
    commit=0.4
    submodules=yes
    srclibs=Otr4j-jitsi@30fbaea9388616b268de82be0267acf2441b218b,BouncyCastle@r1rv50
    rm=libs/bcprov-jdk15on-150.jar,libs/otr4j-0.10.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && \
        cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-150.jar libs/ && \
        pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ package && \
        popd && \
        cp $$Otr4j-jitsi$$/target/otr4j-0.10.jar libs/
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Build:0.4.1,19
    commit=0.4.1
    submodules=yes
    srclibs=Otr4j-jitsi@30fbaea9388616b268de82be0267acf2441b218b,BouncyCastle@r1rv50
    rm=libs/bcprov-jdk15on-150.jar,libs/otr4j-0.10.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && \
        cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-150.jar libs/ && \
        pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ package && \
        popd && \
        cp $$Otr4j-jitsi$$/target/otr4j-0.10.jar libs/
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Build:0.5,21
    commit=0.5
    submodules=yes
    srclibs=Otr4j-jitsi@30fbaea9388616b268de82be0267acf2441b218b,BouncyCastle@r1rv50
    rm=libs/bcprov-jdk15on-150.jar,libs/otr4j-0.10.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && \
        cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-150.jar libs/ && \
        pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ package && \
        popd && \
        cp $$Otr4j-jitsi$$/target/otr4j-0.10.jar libs/
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Build:0.5.1,22
    commit=0.5.1
    submodules=yes
    srclibs=Otr4j-jitsi@30fbaea9388616b268de82be0267acf2441b218b,BouncyCastle@r1rv50
    rm=libs/bcprov-jdk15on-150.jar,libs/otr4j-0.10.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && \
        cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-150.jar libs/ && \
        pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ package && \
        popd && \
        cp $$Otr4j-jitsi$$/target/otr4j-0.10.jar libs/
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Build:0.5.2,23
    commit=0.5.2
    submodules=yes
    srclibs=Otr4j-jitsi@30fbaea9388616b268de82be0267acf2441b218b,BouncyCastle@r1rv50
    rm=libs/bcprov-jdk15on-150.jar,libs/otr4j-0.10.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && \
        cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-150.jar libs/ && \
        pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ package && \
        popd && \
        cp $$Otr4j-jitsi$$/target/otr4j-0.10.jar libs/
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Build:0.6,25
    commit=0.6
    submodules=yes
    srclibs=Otr4j-jitsi@30fbaea9388616b268de82be0267acf2441b218b,BouncyCastle@r1rv50
    rm=libs/bcprov-jdk15on-150.jar,libs/otr4j-0.10.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && \
        cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-150.jar libs/ && \
        pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ package && \
        popd && \
        cp $$Otr4j-jitsi$$/target/otr4j-0.10.jar libs/
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Build:0.7,26
    commit=0.7
    submodules=yes
    srclibs=Otr4j-jitsi@30fbaea9388616b268de82be0267acf2441b218b,BouncyCastle@r1rv50
    rm=libs/bcprov-jdk15on-150.jar,libs/otr4j-0.10.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && \
        cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-150.jar libs/ && \
        pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ package && \
        popd && \
        cp $$Otr4j-jitsi$$/target/otr4j-0.10.jar libs/
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Build:0.7.1,27
    commit=0.7.1
    submodules=yes
    srclibs=Otr4j-jitsi@30fbaea9388616b268de82be0267acf2441b218b,BouncyCastle@r1rv50
    rm=libs/bcprov-jdk15on-150.jar,libs/otr4j-0.10.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && \
        cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-150.jar libs/ && \
        pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ package && \
        popd && \
        cp $$Otr4j-jitsi$$/target/otr4j-0.10.jar libs/
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Build:0.7.2,28
    commit=0.7.2
    submodules=yes
    srclibs=Otr4j-jitsi@30fbaea9388616b268de82be0267acf2441b218b,BouncyCastle@r1rv50
    rm=libs/bcprov-jdk15on-150.jar,libs/otr4j-0.10.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && \
        cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-150.jar libs/ && \
        pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ package && \
        popd && \
        cp $$Otr4j-jitsi$$/target/otr4j-0.10.jar libs/
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Build:0.7.3,31
    commit=0.7.3
    submodules=yes
    srclibs=Otr4j-jitsi@30fbaea9388616b268de82be0267acf2441b218b,BouncyCastle@r1rv50
    rm=libs/bcprov-jdk15on-150.jar,libs/otr4j-0.10.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && \
        cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-150.jar libs/ && \
        pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ package && \
        popd && \
        cp $$Otr4j-jitsi$$/target/otr4j-0.10.jar libs/
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Build:0.8,32
    commit=0.8
    submodules=yes
    srclibs=Otr4j-jitsi@30fbaea9388616b268de82be0267acf2441b218b,BouncyCastle@r1rv50
    rm=libs/bcprov-jdk15on-150.jar,libs/otr4j-0.10.jar
    prebuild=pushd $$BouncyCastle$$ && \
        ant -f ant/jdk15+.xml build-provider && \
        popd && \
        cp $$BouncyCastle$$/build/artifacts/jdk1.5/jars/bcprov-jdk15on-150.jar libs/ && \
        pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ package && \
        popd && \
        cp $$Otr4j-jitsi$$/target/otr4j-0.10.jar libs/
    scanignore=libs/otr4j-0.10.jar,libs/bcprov-jdk15on-150.jar

Build:0.8.2,34
    commit=0.8.2
    gradle=yes
    srclibs=Otr4j-jitsi@6d879761605dba48c80c0d3f44a6555b522d0c2e
    prebuild=pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e '/mavenCentral/amavenLocal()\n' -e '/maven {/d' -e '/jitsi.github.com/,+1d' -e '/buildTypes {/i/*' -e '/subprojects/i*/' build.gradle

Build:0.8.3,35
    commit=0.8.3
    gradle=yes
    srclibs=Otr4j-jitsi@6d879761605dba48c80c0d3f44a6555b522d0c2e
    prebuild=pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e '/mavenCentral/amavenLocal()\n' -e '/maven {/d' -e '/jitsi.github.com/,+1d' -e '/buildTypes {/i/*' -e '/subprojects/i*/' build.gradle

Build:0.8.4,36
    commit=0.8.4
    gradle=yes
    srclibs=Otr4j-jitsi@6d879761605dba48c80c0d3f44a6555b522d0c2e
    prebuild=pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e '/mavenCentral/amavenLocal()\n' -e '/maven {/d' -e '/jitsi.github.com/,+1d' -e '/buildTypes {/i/*' -e '/subprojects/i*/' build.gradle

Build:0.9,37
    commit=0.9
    gradle=yes
    srclibs=Otr4j-jitsi@6d879761605dba48c80c0d3f44a6555b522d0c2e
    prebuild=pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e '/mavenCentral/amavenLocal()\n' -e '/maven {/d' -e '/jitsi.github.com/,+1d' -e '/buildTypes {/i/*' -e '/subprojects/i*/' build.gradle

Build:0.9.1,38
    commit=0.9.1
    gradle=yes
    srclibs=Otr4j-jitsi@6d879761605dba48c80c0d3f44a6555b522d0c2e
    prebuild=pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e '/mavenCentral/amavenLocal()\n' -e '/maven {/d' -e '/jitsi.github.com/,+1d' -e '/buildTypes {/i/*' -e '/subprojects/i*/' build.gradle

Build:0.9.2,39
    commit=0.9.2
    gradle=yes
    srclibs=Otr4j-jitsi@6d879761605dba48c80c0d3f44a6555b522d0c2e
    prebuild=pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e '/mavenCentral/amavenLocal()\n' -e '/maven {/d' -e '/jitsi.github.com/,+1d' -e '/buildTypes {/i/*' -e '/subprojects/i*/' build.gradle

Build:0.9.3,40
    commit=0.9.3
    gradle=yes
    srclibs=Otr4j-jitsi@6d879761605dba48c80c0d3f44a6555b522d0c2e
    prebuild=pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e '/mavenCentral/amavenLocal()\n' -e '/maven {/d' -e '/jitsi.github.com/,+1d' -e '/buildTypes {/i/*' -e '/subprojects/i*/' build.gradle

Build:0.10,41
    commit=0.10
    gradle=yes
    srclibs=Otr4j-jitsi@6d879761605dba48c80c0d3f44a6555b522d0c2e
    prebuild=pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e '/mavenCentral/amavenLocal()\n' -e '/maven {/d' -e '/jitsi.github.com/,+1d' -e '/applicationVariants/i/*' -e '/lintOptions {/i*/' build.gradle

Build:1.0.1,46
    commit=1.0.1
    gradle=yes
    srclibs=Otr4j-jitsi@8bc49a960b995ab79583df592abe7a5066599491
    prebuild=pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e '/mavenCentral/amavenLocal()\n' -e '/maven {/d' -e '/jitsi.github.com/,+1d' -e '/applicationVariants/i/*' -e '/lintOptions {/i*/' build.gradle

Build:1.0.3,48
    commit=1.0.3
    gradle=yes
    srclibs=Otr4j-jitsi@8bc49a960b995ab79583df592abe7a5066599491
    prebuild=pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e '/mavenCentral/amavenLocal()\n' -e '/maven {/d' -e '/jitsi.github.com/,+1d' -e '/applicationVariants/i/*' -e '/lintOptions {/i*/' build.gradle

Build:1.1.0-beta,51
    commit=1.1.0-beta
    gradle=yes
    srclibs=Otr4j-jitsi@8bc49a960b995ab79583df592abe7a5066599491
    prebuild=pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e '/mavenCentral/amavenLocal()\n' -e '/maven {/d' -e '/jitsi.github.com/,+1d' -e '/applicationVariants/i/*' -e '/lintOptions {/i*/' build.gradle

Build:1.1.0,53
    commit=1.1.0
    gradle=yes
    srclibs=Otr4j-jitsi@8bc49a960b995ab79583df592abe7a5066599491
    prebuild=pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e '/mavenCentral/amavenLocal()\n' -e '/maven {/d' -e '/jitsi.github.com/,+1d' -e '/applicationVariants/i/*' -e '/lintOptions {/i*/' build.gradle

Build:1.2.0-beta,54
    commit=1.2.0-beta
    gradle=yes
    srclibs=Otr4j-jitsi@8bc49a960b995ab79583df592abe7a5066599491
    prebuild=pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e '/mavenCentral/amavenLocal()\n' -e '/maven {/d' -e '/jitsi.github.com/,+1d' -e '/applicationVariants/i/*' -e '/lintOptions {/i*/' build.gradle

Build:1.2.0,56
    commit=1.2.0
    gradle=yes
    srclibs=Otr4j-jitsi@8bc49a960b995ab79583df592abe7a5066599491
    prebuild=pushd $$Otr4j-jitsi$$ && \
        $$MVN3$$ clean && \
        $$MVN3$$ install && \
        popd && \
        sed -i -e '/mavenCentral/amavenLocal()\n' -e '/maven {/d' -e '/jitsi.github.com/,+1d' -e '/applicationVariants/i/*' -e '/lintOptions {/i*/' build.gradle

Maintainer Notes:
Build succeeds now, but fdroid strips a line required for output variants.
The current solution is to comment out buildTypes {} and lintOptions {}, but
we should fix this or at least narrow it down. Enabling for now.
.

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:1.2.0
Current Version Code:56

